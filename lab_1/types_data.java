public class Types {
    // целочисленные типы данных различаются диапазонами значений
    public static void main(String[] args) {
    //состоит из 8 бит
    System.out.println ("Минимальное значение типа byte = " + Byte.MIN_VALUE);
    System.out.println ("Максимальное значение типа byte = " + Byte.MAX_VALUE);
    //состоит из 16 бит
    System.out.println ("Минимальное значение типа short = " + Short.MIN_VALUE);
    System.out.println ("Максимальное значение типа short = " + Short.MAX_VALUE);
    //состоит из 32 бит
    System.out.println ("Минимальное значение типа int = " + Integer.MIN_VALUE);
    System.out.println ("Максимальное значение типа int = " + Integer.MAX_VALUE);
    //состоит из 64 бит
    System.out.println ("Минимальное значение типа long = " + Long.MIN_VALUE);
    System.out.println ("Максимальное значение типа long = " + Long.MAX_VALUE);
    System.out.println ("Минимальное значение типа float = " + Float.MIN_VALUE);
    System.out.println ("Максимальное значение типа float = " + Float.MAX_VALUE);
    System.out.println ("Минимальное значение типа double = " + Double.MIN_VALUE);
    System.out.println ("Максимальное значение типа double = " + Double.MAX_VALUE);
    }
    }
    
    
    