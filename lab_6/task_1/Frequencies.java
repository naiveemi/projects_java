package lab_6.task_1;

import java.util.Comparator;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Frequencies {
    public static void main(String[] args) {
        try (var scanner = new Scanner(System.in)) {
            System.out.println("Введите строку: ");
            var input = scanner.nextLine();
            input.chars()
                .mapToObj(x -> (char) x)
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .entrySet()
                .stream()
                .max(Comparator.comparing(Entry::getValue))
                .stream().forEach(x -> System.out.println(x));
        }
    }
}


