package DNS.lab9.entity;

public class Order extends AbstractEntity<Integer> {
    private Integer id;
    private Integer cost;
    private String products;

    public Order(Integer id, Integer cost, String products) {
        this.id = id;
        this.cost = cost;
        this.products = products;
    }
    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "[" + id + "] cost =" + cost + ",products =" + products;
    }
}

