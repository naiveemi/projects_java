package DNS.lab9.entity;

import java.io.Serializable;

    public interface Identifiable<E extends Serializable> {
        E getId();
    }
    

