package DNS.lab9;

import DNS.lab9.repository.AccountRepository;
import DNS.lab9.repository.OrderRepository;
import DNS.lab9.repository.ProductRepository;
import DNS.lab9.repository.impl.AccountRepositoryImpl;
import DNS.lab9.repository.impl.OrderRepositoryImpl;
import DNS.lab9.repository.impl.ProductRepositoryImpl;

import java.io.IOException;
import java.util.Scanner;

public class DNSApp {
    private static final AccountRepository ACCOUNT_REPOSITORY = AccountRepositoryImpl.getInstance();
    private static final OrderRepository ORDER_REPOSITORY = OrderRepositoryImpl.getInstance();
    private static final ProductRepository PRODUCT_REPOSITORY = ProductRepositoryImpl.getInstance();


    public static void main(String[] args) throws IOException {
        try (Scanner in = new Scanner(System.in)) {
            System.out.print("1 - Найти пользователя по id,  2 - Найти пользователя по имени, 3 - Найти продукт по id, 4 - Найти продукт по названию, 5 - Найти заказ по id\n");
            int choice = in.nextInt();
            in.nextLine();
            switch (choice) {
                case 1 -> {
                    System.out.println("Введите id пользователя: ");
                    int idAccount = in.nextInt();
                    System.out.println(ACCOUNT_REPOSITORY.findAccountById(idAccount));
                }
                case 2 -> {
                    System.out.println("Введите имя пользователя: ");
                    String nameAccount = in.nextLine();
                    System.out.println(ACCOUNT_REPOSITORY.findAccountByName(nameAccount));
                }
                case 3 -> {
                    System.out.println("Введите id продукта: ");
                    int idProduct = in.nextInt();
                    System.out.println(PRODUCT_REPOSITORY.findProductById(idProduct));
                }
                case 4 -> {
                    System.out.println("Введите название продукта: ");
                    String nameProduct = in.nextLine();
                    System.out.println(PRODUCT_REPOSITORY.findProductByName(nameProduct));
                }
                case 5 -> {
                    System.out.println("Введите id заказа: ");
                    int idOrder = in.nextInt();
                    System.out.println(ORDER_REPOSITORY.findOrderById(idOrder));
                }
            }
        }
    }
}


