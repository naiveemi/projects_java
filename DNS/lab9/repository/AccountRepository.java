package DNS.lab9.repository;

import java.util.ArrayList;

import DNS.lab9.entity.Account;

public interface AccountRepository {
    Account findAccountById(Integer id);
    
    Account findAccountByName(String name);

    ArrayList<Account> findAllAccounts();
}

