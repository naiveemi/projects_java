package DNS.lab9.repository;

import java.util.ArrayList;

import DNS.lab9.entity.Order;

public interface OrderRepository {
    Order findOrderById(Integer id);

    ArrayList<Order> findAllOrders();
}

