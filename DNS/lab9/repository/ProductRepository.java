package DNS.lab9.repository;

import java.util.ArrayList;

import DNS.lab9.entity.Product;

public interface ProductRepository {
    Product findProductById(Integer id);
    
    Product findProductByName(String name);

    Product findProductByCost(String cost);

    ArrayList<Product> findAllProducts();
}

