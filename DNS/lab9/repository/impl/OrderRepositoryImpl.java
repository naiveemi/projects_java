package DNS.lab9.repository.impl;

import DNS.lab9.entity.Order;

import DNS.lab9.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class OrderRepositoryImpl implements OrderRepository {
    private static final ArrayList<Order> STATE = new ArrayList<>();
    private static OrderRepository instance;

    static {
        STATE.addAll(List.of(
                new Order(1, 200, "hairdryer"),
                new Order(2, 400, "mouse, headphones"),
                new Order(3, 10, "headphones")
        ));
    }

    public static synchronized OrderRepository getInstance() {
        if (isNull(instance)) {
            instance = new OrderRepositoryImpl();
        }
        return instance;
    }

    @Override
    public synchronized Order findOrderById(Integer id) {
        for (Order Order : STATE) {
            if (Order.getId() == id)
                return Order;
        }
        return null;
    }

    @Override
    public synchronized ArrayList<Order> findAllOrders() {
        return STATE;
    }

}

