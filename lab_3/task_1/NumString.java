import java.util.Scanner;
public class NumString{
    public static void main(String args []) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите строки через пробел:");
            String str = scanner.nextLine();
            System.out.println("Введите подстроку:");
            String substring = scanner.nextLine();
            System.out.println(searchMatches(str, substring));
        }    
    }

    private static int searchMatches(String str, String substring) {
        int counter = 0, i = -1;
        while ((i = str.indexOf(substring, i + 1)) > -1) ++counter;
    return counter;
    }
}