import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class SensorReadings {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Введите показания датчиков: ");
            String readings = scanner.nextLine();
            System.out.print("Выводить по(1 - возрастанию id, 2 - возрастанию средней температуры): ");
            int mode = scanner.nextInt();
            Integer[][] result = OutputReadings(readings);
            switch (mode) {
                case 1:
                    Arrays.sort(result, Comparator.comparingInt(o -> o[0]));
                    for (int i = 0; i < result.length; i++) 
                    System.out.println(result[i][0] + " " + result[i][1]);
                break;
                case 2:
                    Arrays.sort(result, Comparator.comparingInt(o -> o[1]));
                    for (int i = 0; i < result.length; i++) 
                    System.out.println(result[i][0] + " " + result[i][1]); 
                break;
                }
        }
        
    }
    public static Integer[][] OutputReadings(String readings) {
        String[] text = readings.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++){
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2, text[i].length()));
        }
        return output;  
    }
}

    

