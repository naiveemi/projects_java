import java.util.Scanner;

public class ReplacingCharacters {
    public static void main(String args[]) {
        try (Scanner scanner = new Scanner(System.in, "cp866")) {
            System.out.println("Введите строку:");
            String text = scanner.nextLine();
            String value = text.replaceAll("(.)\\1{2,}", "$1");  
            System.out.println("Верное написание: " + value); 
        }
    }
}
