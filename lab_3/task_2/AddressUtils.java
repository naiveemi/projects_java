import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddressUtils {
  private static final String ADDRESS_REGEX = "^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$";
	private static final Pattern ADDRESS_PATTERN = Pattern.compile(ADDRESS_REGEX);

	public static boolean addressValidator(String address)
	{
		if (address == null) {
			return false;
		}

		Matcher matcher = ADDRESS_PATTERN.matcher(address);
		return matcher.matches();
	}

	public static void main(String[] args)
	{
		try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите адрес IPv6:");
            String address = scanner.nextLine();

            if (addressValidator(address)) {
            	System.out.println(address + ": адрес IPv6");
            }
            else {
            	System.out.println(address + ": не является адресом IPv6");
            }
        }
	}
}