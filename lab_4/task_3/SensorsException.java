public class SensorsException extends Exception {
    public SensorsException(Throwable cause) {
        super(cause);
    }
    
    public SensorsException (String message) {
        super(message);
    }
    
    public SensorsException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public SensorsException() {
        super();
    }
    
    @Override
    public String toString() {
        return "SensorsException: " + this.getMessage();
    }
}
    