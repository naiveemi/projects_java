import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
public class SensorsReadings {
    public static void main(String[] args) {
        try {
            try (Scanner input = new Scanner(System.in)) {
                System.out.print("Введите показания датчиков: ");
                String read = input.nextLine();
                System.out.print("Введите 1, если хотите сортировать по id и 2, если по температуре ");
                int mode = input.nextInt();
                Integer[][] result = outputFromSensors(read);
                if (mode == 1) {
                    Arrays.sort(result, Comparator.comparingDouble(o -> o[0]));
                    for (int i = 0; i < result.length; i++)
                        System.out.println(result[i][0] + " " + result[i][1]);
                } else if (mode == 2) {
                    Arrays.sort(result, Comparator.comparingDouble(o -> o[1]));
                    for (int i = 0; i < result.length; i++)
                        System.out.println(result[i][0] + " " + result[i][1]);
                } else {
                    System.out.println("Введите 1 или 2");
                }
            }
        } catch (SensorsException e) {
            System.out.println(e);
        }
    }

    public static Integer[][] outputFromSensors(String read) throws SensorsException {
        if (!read.contains("@")) {
            throw new SensorsException();
        }
        try {
            String[] str = read.split("@");
            Integer[][] output = new Integer[str.length][2];
            for (int i = 0; i < str.length; i++) {
                output[i][0] = Integer.parseInt(str[i].substring(0, 2));
                output[i][1] = Integer.parseInt(str[i].substring(2, str[i].length()));
            }
            return output;
        } catch (Exception e) {
            throw new SensorsException("Неверный ввод");
        }

    }
}


    
