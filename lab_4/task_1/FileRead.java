package lab_4;
import java.io.*;
import java.util.Scanner;


public class FileRead {
    public static void main(String[] args) throws IOException {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите путь: ");
            String fileName = scanner.nextLine();
            System.out.println(readUsingFileReader(fileName));        
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
        }
        }
    private static String readUsingFileReader(String fileName) throws IOException {
        File file = new File(fileName);
        String text = "";
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while((line = br.readLine()) != null){
            text += new String(line);
        }
    
        br.close();
        fr.close();
        return text; 
    }
}