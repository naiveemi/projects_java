import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Введите число, его тип и в какой тип преобразовать через пробел: ");
            String str = scanner.nextLine();
            String[] words = str.split(" ");
            String data = words[0];
            String inputType = words[1];
            String desiredType = words[2];
            int dataInt;
            float dataFloat;
            double dataDouble;
            long dataLong;
            short dataShort;
            try {
                switch (inputType) {
                    case "int":
                        dataInt = Integer.parseInt(data);
                        switch (desiredType) {
                            case "float":
                                dataFloat = (float) dataInt;
                                System.out.println(dataFloat);
                                break;
                            case "double":
                                dataDouble = (double) dataInt;
                                System.out.println(dataDouble);
                                break;
                            case "long":
                                dataLong = (long) dataInt;
                                System.out.println(dataLong);
                                break;
                            case "short":
                                dataShort = (short) dataInt;
                                System.out.println(dataShort);
                                break;
                        }
                        break;
                    case "float":
                        dataFloat = Float.parseFloat(data);
                        switch (desiredType) {
                            case "int":
                                dataInt = (int) dataFloat;
                                System.out.println(dataInt);
                                break;
                            case "double":
                                dataDouble = (double) dataFloat;
                                System.out.println(dataDouble);
                                break;
                            case "long":
                                dataLong = (long) dataFloat;
                                System.out.println(dataLong);
                                break;
                            case "short":
                                dataShort = (short) dataFloat;
                                System.out.println(dataShort);
                                break;
                        }
                        break;
                    case "double":
                        dataDouble = Double.parseDouble(data);
                        switch (desiredType) {
                            case "int":
                                dataInt = (int) dataDouble;
                                System.out.println(dataInt);
                                break;
                            case "float":
                                dataFloat = (float) dataDouble;
                                System.out.println(dataDouble);
                                break;
                            case "long":
                                dataLong = (long) dataDouble;
                                System.out.println(dataLong);
                                break;
                            case "short":
                                dataShort = (short) dataDouble;
                                System.out.println(dataShort);
                                break;
                        }
                        break;
                    case "long":
                        dataLong = Long.parseLong(data);
                        switch (desiredType) {
                            case "int":
                                dataInt = (int) dataLong;
                                System.out.println(dataInt);
                                break;
                            case "double":
                                dataDouble = (double) dataLong;
                                System.out.println(dataDouble);
                                break;
                            case "float":
                                dataFloat = (float) dataLong;
                                System.out.println(dataFloat);
                                break;
                            case "short":
                                dataShort = (short) dataLong;
                                System.out.println(dataShort);
                                break;
                        }
                        break;
                    case "short":
                        dataShort = Short.parseShort(data);
                        switch (desiredType) {
                            case "int":
                                dataInt = (int) dataShort;
                                System.out.println(dataInt);
                                break;
                            case "double":
                                dataDouble = (double) dataShort;
                                System.out.println(dataDouble);
                                break;
                            case "long":
                                dataLong = (long) dataShort;
                                System.out.println(dataLong);
                                break;
                            case "float":
                                dataFloat = (float) dataShort;
                                System.out.println(dataFloat);
                                break;
                        }
                        break;
                }
            } catch (Exception e) {
                System.out.println("Ошибка перевода");
            }
        }
    }
}
