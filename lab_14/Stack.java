package lab14;

public interface Stack<T> {
    void push(T t);

    T pop();

    boolean isEmpty();
}
