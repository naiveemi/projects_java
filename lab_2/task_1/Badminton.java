import java.util.Scanner;
public class Badminton {
    public static void main(String[] strings) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Сегодня воскресенье? Да - 1, Нет -2: ");
            int choice = scanner.nextInt();
            if (choice == 1){
                System.out.print("Какая температура? Жарко - 1, Тепло - 2, Холодно - 3:  ");
                int temperature = scanner.nextInt();
                if (temperature == 2){
                    System.out.print("Каковы осадки? Ясно - 1, Облачно - 2, Дождь - 3, Снег - 4, Град - 5:  ");
                    int precipitation= scanner.nextInt();
                if (precipitation == 1)
                    System.out.print("Есть ли ветер? Есть - 1, Нет - 2: ");
                    int wind= scanner.nextInt();
                if (wind == 2)
                    System.out.print("Какова влажность? Высокая - 1, Низкая - 2: ");
                    int humidity= scanner.nextInt();
                if (humidity == 2)
                    System.out.print("Да");
            }else
                System.out.print("Нет");
            }
        }
    }
}
