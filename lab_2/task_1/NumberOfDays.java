import java.util.Scanner;
public class NumberOfDays {
    public static void main(String[] strings) {
        Scanner scanner = new Scanner(System.in);
        int number_Of_DaysInMonth = 0; 
        String MonthOfName ="Unknown";
        System.out.print("Введите номер месяца: ");
        int month = scanner.nextInt();
        System.out.print("Введите год: ");
        int year = scanner.nextInt();
        switch (month) {
            case 1:
                MonthOfName ="В январе";
                number_Of_DaysInMonth = 31;
                break;
                case 2:
                MonthOfName ="В феврале";
                if ((year % 400 == 0) || ((year % 4 == 0) 
                && (year % 100 != 0))) {
                    number_Of_DaysInMonth = 29;
                } else {
                    number_Of_DaysInMonth = 28;
                }
                break;
            case 3:
                MonthOfName ="В марте";
                number_Of_DaysInMonth = 31;
                break;
            case 4:
                MonthOfName ="В апреле";
                number_Of_DaysInMonth = 30;
                break;
            case 5:
                MonthOfName ="В мае";
                number_Of_DaysInMonth = 31;
                break;
            case 6:
                MonthOfName ="В июне";
                number_Of_DaysInMonth = 30;
                break;
            case 7:
                MonthOfName ="В июле";
                number_Of_DaysInMonth = 31;
                break;
            case 8:
                MonthOfName ="В августе";
                number_Of_DaysInMonth = 31;
                break;
            case 9:
                MonthOfName ="В сентябре";
                number_Of_DaysInMonth = 30;
                break;
            case 10:
                MonthOfName ="В октябре";
                number_Of_DaysInMonth = 31;
                break;
            case 11:
                MonthOfName ="В ноябре";
                number_Of_DaysInMonth = 30;
                break;
            case 12:
                MonthOfName ="В декабре";
                number_Of_DaysInMonth = 31;
    }
    System.out.print(MonthOfName + " " + year + " года " 
    + number_Of_DaysInMonth + " дней\n");
    scanner.close();
}


}  
