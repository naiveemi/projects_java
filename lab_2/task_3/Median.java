import java.util.Arrays;

public class Median {
    public static void main(String[] args) {
        double array[] = {5, 15, 512, 125, 95, 9, 10, 11, 124, 2000};
        Arrays.sort(array);
        double median;
        if (array.length % 2 == 0)
            median = ((double)array[array.length/2] + (double)array[array.length/2 - 1])/2;
        else
            median = (double) array[array.length/2];
            System.out.println("Медиана: " + median);
        }
    }