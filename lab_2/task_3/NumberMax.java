
public class NumberMax {
    public static void main(String[] args) {
            double array[] = {346, 1423, 1125, 4152, 4211, 412, 914, 4211, 124, 41, 4211};
            double max = Integer.MIN_VALUE;
            int count = 0;
            for (int i = 0; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                    count = 1;
                } else if (array[i] == max) {
                    count++;
                }
            }
            System.out.println("Количество чисел, равных максимальному: " + count);
            System.out.println("Максимальное число: " + max);
        }
    }
  
