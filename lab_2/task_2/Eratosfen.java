public class Eratosfen {
    public static void sieveOfEratosthenes(int n)
    {
        
        int[] a = new int[n + 1];
 
        for (int i = 0; i <= n; i++) {      
            a[i] = 1;
        }
 
        for (int i = 2; i <= Math.sqrt(n); i++)
        {
            if (a[i] == 1)                  
            {
                for (int j = 2; i * j <= n; j++) {
                    a[i * j] = 0;          
                }
            }
        }
 
        for (int i = 2; i <= n; i++)
        {
            if (a[i] == 1) {
                System.out.print(i + " "); 
            }
        }
    }
 
    public static void main(String[] args)
    {
        // вывести простые числа меньше 100
        sieveOfEratosthenes(100);
    }
}