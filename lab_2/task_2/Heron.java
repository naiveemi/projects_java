import java.util.Scanner;
public class Heron {
    public static void main(String[] strings) {
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Введите число: ");
            int a = scanner.nextInt();
            double b=a;
            int i=0;
            while ((b*b>a)&&(i<200)){
                b=(b+a/b)/2;
                i++;
            }
            System.out.print("Значение квадратного корня: " + b);
        }
    }
        
}
